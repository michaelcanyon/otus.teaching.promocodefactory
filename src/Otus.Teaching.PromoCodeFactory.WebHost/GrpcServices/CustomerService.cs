﻿using Grpc.Core;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Hubs;
using Otus.Teaching.PromoCodeFactory.WebHost.protos;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GrpcServices
{
    public class CustomerService : CustomerProto.CustomerProtoBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IHubContext<CustomerStatusHub> _customerStatusHub;

        public CustomerService(IRepository<Customer> customerRepository, IHubContext<CustomerStatusHub> customerStatusHub)
        {
            _customerRepository = customerRepository;
            _customerStatusHub = customerStatusHub;
        }

        public override async Task<GetCustomersResponse> GetCustomers(GetCustomersRequest request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var protoCustomers = customers
                .Select(c => new CustomerShortResponse
                {
                    Id = c.Id.ToString(),
                    Email = c.Email,
                    FirstName = c.Email,
                    LastName = c.LastName
                }).ToList();
            var result = new GetCustomersResponse { Customers = { protoCustomers } };
            return result;
        }

        public override async Task<CreateCustomerResponse> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            var newCustomer = new Customer
            {
                Id = Guid.NewGuid(),
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };
            await _customerRepository.CreateEntity(newCustomer);
            await _customerStatusHub.Clients.All.SendAsync("ReceiveStatus", $"{newCustomer.Id} customer Created! Values: {newCustomer.FullName}, {newCustomer.Email} \n");
            var result = new CreateCustomerResponse { Result = true };
            return result;
        }

        public override async Task<UpdateCustomerResponse> UpdateCustomer(UpdateCustomerRequest request, ServerCallContext context)
        {
            Guid.TryParse(request.Id, out Guid customerId);
            var newCustomer = new Customer
            {
                Id = customerId,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };
            await _customerRepository.UpdateEntity(newCustomer);
            await _customerStatusHub.Clients.All.SendAsync("ReceiveStatus", $"{newCustomer.Id} customer updated! new values: {newCustomer.FullName}, {newCustomer.Email} \n");
            var result = new UpdateCustomerResponse { Result = true };
            return result;
        }

        public override async Task<DeleteCustomerResponse> DeleteCustomer(DeleteCustomerRequest request, ServerCallContext context)
        {
            Guid.TryParse(request.Id, out Guid customerId);
            await _customerRepository.DeleteEntity(customerId);
            await _customerStatusHub.Clients.All.SendAsync("ReceiveStatus", $"{request.Id} customer Deleted!\n");
            return new DeleteCustomerResponse() { Result = true };
        }

        public override async Task<GetCustomerByIdResponse> GetCustomerById(GetCustomerByIdRequest request, ServerCallContext context)
        {
            Guid.TryParse(request.Id.ToString(), out Guid customerId);
            var customer = await _customerRepository.GetByIdAsync(customerId);
            return new GetCustomerByIdResponse()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };

        }
    }
}
