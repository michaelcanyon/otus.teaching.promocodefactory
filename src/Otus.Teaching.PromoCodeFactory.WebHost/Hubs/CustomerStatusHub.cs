﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{
    public class CustomerStatusHub : Hub
    {
        public async Task SendStatusState(string message)
        {
            await Clients.All.SendAsync("ReceiveStatus", message);
        }
    }
}
